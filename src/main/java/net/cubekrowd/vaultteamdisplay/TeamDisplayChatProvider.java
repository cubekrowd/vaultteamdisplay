/*

    messagekrowd
    Copyright (C) 2017  CubeKrowd Network

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package net.cubekrowd.vaultteamdisplay;

import java.lang.*;
import java.util.*;
import java.io.*;
import org.bukkit.*;
import org.bukkit.entity.*;
import org.bukkit.plugin.*;
import org.bukkit.plugin.java.*;
import net.milkbowl.vault.chat.Chat;
import com.google.common.io.*;
import org.bukkit.event.*;
import org.bukkit.event.player.*;
import org.bukkit.scheduler.*;
import org.bukkit.plugin.messaging.PluginMessageListener;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.scoreboard.Team;
import org.bukkit.event.*;
import org.bukkit.event.player.*;
import org.bukkit.scheduler.*;
import org.bukkit.plugin.messaging.PluginMessageListener;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.scoreboard.Team;

public class TeamDisplayChatProvider extends net.milkbowl.vault.chat.Chat {

    private final VTDPlugin plugin;
    private final net.milkbowl.vault.permission.Permission perms;

    TeamDisplayChatProvider(VTDPlugin plugin, net.milkbowl.vault.permission.Permission perms) {
        super(perms);
        this.plugin = plugin;
        this.perms = perms;
    }

    @Override
    public String getName() {
        return this.plugin.getName();
    }

    @Override
    public boolean isEnabled() {
        return this.plugin.isEnabled();
    }

    private Set<Map.Entry<String, String>> contextsFrom(String world) {
        throw new UnsupportedOperationException("Not implemented!");
    }

    @Override
    public String getGroupInfoString(final String world, String name, final String key, String defaultValue) {
        throw new UnsupportedOperationException("Not implemented!");
    }

    @Override
    public void setGroupInfoString(final String world, String name, final String key, final String value) {
        throw new UnsupportedOperationException("Not implemented!");
    }


    @Override
    public String getPlayerInfoString(String world, OfflinePlayer player, String node, String defaultValue) {
        throw new UnsupportedOperationException("Not implemented!");
    }

    @Override
    public void setPlayerInfoString(final String world, OfflinePlayer player, final String node, final String value) {
        throw new UnsupportedOperationException("Not implemented!");
    }

    // -- Passthrough methods

    @Override
    public String getGroupPrefix(String world, String name) {
        throw new UnsupportedOperationException("Not implemented!");
    }

    @Override
    public void setGroupPrefix(String world, String name, String prefix) {
        setGroupInfoString(world, name, "prefix", prefix);
    }

    @Override
    public String getGroupSuffix(String world, String name) {
        throw new UnsupportedOperationException("Not implemented!");
    }

    @Override
    public void setGroupSuffix(String world, String name, String suffix) {
        setGroupInfoString(world, name, "suffix", suffix);
    }

    @Override
    public int getGroupInfoInteger(String world, String name, String node, int defaultValue) {
        try {
            return Integer.parseInt(getGroupInfoString(world, name, node, String.valueOf(defaultValue)));
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    @Override
    public void setGroupInfoInteger(String world, String name, String node, int value) {
        setGroupInfoString(world, name, node, String.valueOf(value));
    }


    @Override
    public double getGroupInfoDouble(String world, String name, String node, double defaultValue) {
        try {
            return Double.parseDouble(getGroupInfoString(world, name, node, String.valueOf(defaultValue)));
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    @Override
    public void setGroupInfoDouble(String world, String name, String node, double value) {
        setGroupInfoString(world, name, node, String.valueOf(value));
    }


    @Override
    public boolean getGroupInfoBoolean(String world, String name, String node, boolean defaultValue) {
        return Boolean.parseBoolean(getGroupInfoString(world, name, node, String.valueOf(defaultValue)));
    }

    @Override
    public void setGroupInfoBoolean(String world, String name, String node, boolean value) {
        setGroupInfoString(world, name, node, String.valueOf(value));
    }


    @Override
    public String getPlayerPrefix(String world, OfflinePlayer oplayer) {
        if (oplayer.getPlayer().getScoreboard() != null) {
            A : for (Team team : oplayer.getPlayer().getScoreboard().getTeams()) {
                //System.out.println(team.getName());
                for (String entry : team.getEntries()) {
                    //System.out.println("=" + entry);
                    if(entry.equalsIgnoreCase(oplayer.getPlayer().getName())) {
                        return team.getPrefix();
                    }
                }
            }

        }
        return "";
    }

    @Override
    public void setPlayerPrefix(String world, OfflinePlayer player, String prefix) {
        setPlayerInfoString(world, player, "prefix", prefix);
    }

    @Override
    public String getPlayerSuffix(String world, OfflinePlayer oplayer) {
        if (oplayer.getPlayer().getScoreboard() != null) {
            A : for (Team team : oplayer.getPlayer().getScoreboard().getTeams()) {
                //System.out.println(team.getName());
                for (String entry : team.getEntries()) {
                    //System.out.println("=" + entry);
                    if(entry.equalsIgnoreCase(oplayer.getPlayer().getName())) {
                        return team.getSuffix();
                    }
                }
            }

        }
        return "";
    }

    @Override
    public void setPlayerSuffix(String world, OfflinePlayer player, String suffix) {
        setPlayerInfoString(world, player, "suffix", suffix);
    }

    @Override
    public int getPlayerInfoInteger(String world, OfflinePlayer player, String node, int defaultValue) {
        try {
            return Integer.parseInt(getPlayerInfoString(world, player, node, String.valueOf(defaultValue)));
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    @Override
    public void setPlayerInfoInteger(String world, OfflinePlayer player, String node, int value) {
        setPlayerInfoString(world, player, node, String.valueOf(value));
    }

    @Override
    public double getPlayerInfoDouble(String world, OfflinePlayer player, String node, double defaultValue) {
        try {
            return Double.parseDouble(getPlayerInfoString(world, player, node, String.valueOf(defaultValue)));
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    @Override
    public void setPlayerInfoDouble(String world, OfflinePlayer player, String node, double value) {
        setPlayerInfoString(world, player, node, String.valueOf(value));
    }

    @Override
    public boolean getPlayerInfoBoolean(String world, OfflinePlayer player, String node, boolean defaultValue) {
        return Boolean.parseBoolean(getPlayerInfoString(world, player, node, String.valueOf(defaultValue)));
    }

    @Override
    public void setPlayerInfoBoolean(String world, OfflinePlayer player, String node, boolean value) {
        setPlayerInfoString(world, player, node, String.valueOf(value));
    }

    // -- Deprecated stuff
    @SuppressWarnings("deprecation")
    private OfflinePlayer pFromName(String name) {
        return plugin.getServer().getOfflinePlayer(name);
    }

    @Override
    public int getPlayerInfoInteger(String world, String name, String node, int defaultValue) {
        return getPlayerInfoInteger(world, pFromName(name), node, defaultValue);
    }

    @Override
    public void setPlayerInfoInteger(String world, String name, String node, int value) {
        setPlayerInfoInteger(world, pFromName(name), node, value);
    }

    @Override
    @Deprecated
    public String getPlayerInfoString(String world, String name, String node, String defaultValue) {
        return getPlayerInfoString(world, pFromName(name), node, defaultValue);
    }

    @Override
    public void setPlayerInfoString(String world, String name, String node, String value) {
        setPlayerInfoString(world, pFromName(name), node, value);
    }

    @Override
    public boolean getPlayerInfoBoolean(String world, String name, String node, boolean defaultValue) {
        return getPlayerInfoBoolean(world, pFromName(name), node, defaultValue);
    }

    @Override
    public void setPlayerInfoBoolean(String world, String name, String node, boolean value) {
        setPlayerInfoBoolean(world, pFromName(name), node, value);
    }

    @Override
    public double getPlayerInfoDouble(String world, String name, String node, double defaultValue) {
        return getPlayerInfoDouble(world, pFromName(name), node, defaultValue);
    }

    @Override
    public void setPlayerInfoDouble(String world, String name, String node, double value) {
        setPlayerInfoDouble(world, pFromName(name), node, value);
    }

    @Override
    public String getPlayerPrefix(String world, String name) {
        return getPlayerPrefix(world, pFromName(name));
    }

    @Override
    public void setPlayerPrefix(String world, String name, String prefix) {
        setPlayerPrefix(world, pFromName(name), prefix);
    }

    @Override
    public String getPlayerSuffix(String world, String name) {
        return getPlayerSuffix(world, pFromName(name));
    }

    @Override
    public void setPlayerSuffix(String world, String name, String suffix) {
        setPlayerSuffix(world, pFromName(name), suffix);
    }
}
