# VaultTeamDisplay
![VaultTeamDisplay](https://gitlab.com/cubekrowd/vaultteamdisplay/raw/master/cover.png "VaultTeamDisplay")

[![build status](https://gitlab.com/cubekrowd/vaultteamdisplay/badges/master/build.svg)](https://gitlab.com/cubekrowd/vaultteamdisplay/pipelines)
![license](https://img.shields.io/badge/license-AGLP%203.0-blue.svg?style=flat)

## About

VaultTeamDisplay is a small utility plugin which hooks into vault and displays a players scoreboard as prefix.  
There are no commands, permissions nor configs. Just drag and drop and it should work.  
Please report any error messages [on the issue tracker here](https://gitlab.com/cubekrowd/vaultteamdisplay/issues).

## Links
* [Source Code](https://gitlab.com/cubekrowd/vaultteamdisplay)
* [Issue Tracker](https://gitlab.com/cubekrowd/vaultteamdisplay/issues)

This plugin was created for [CubeKrowd](https://cubekrowd.net/).